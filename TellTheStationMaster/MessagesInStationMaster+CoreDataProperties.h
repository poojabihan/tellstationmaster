//
//  MessagesInStationMaster+CoreDataProperties.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "MessagesInStationMaster+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MessagesInStationMaster (CoreDataProperties)

+ (NSFetchRequest<MessagesInStationMaster *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *msgtype;
@property (nullable, nonatomic, copy) NSString *msgdetail;
@property (nullable, nonatomic, copy) NSString *lastmsg;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *changeitid;

@end

NS_ASSUME_NONNULL_END
