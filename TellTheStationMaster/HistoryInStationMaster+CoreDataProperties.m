//
//  HistoryInStationMaster+CoreDataProperties.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "HistoryInStationMaster+CoreDataProperties.h"

@implementation HistoryInStationMaster (CoreDataProperties)

+ (NSFetchRequest<HistoryInStationMaster *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HistoryInStationMaster"];
}

@dynamic msgdetail;
@dynamic locationdetail;
@dynamic image;
@dynamic createddate;
@dynamic changeitid;

@end
