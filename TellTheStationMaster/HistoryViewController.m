//
//  HistoryViewController.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"
#import "DetailViewController.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "ViewController.h"
#import "HistoryInStationMaster+CoreDataProperties.h"
#import "ChatViewController.h"
#import "constant.h"
#import "UIImageView+WebCache.h"



@interface HistoryViewController ()
{
    NSArray *dictArray,*fetchedObjects;
    NSMutableDictionary *dictForTV;
    MBProgressHUD *hud;
    NSString * historyURL,*imageURLHist;
    
}
@end
NSManagedObjectContext *historyContext;
@implementation HistoryViewController


#pragma mark ViewLifeCycle Delgates
//- (void) dealloc
//{
//    // If you don't remove yourself as an observer, the Notification Center
//    // will continue to try and send notification objects to the deallocated
//    // object.
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//    //[super dealloc];
//}

//- (id) init
//{
//    self = [super init];
//    if (!self) return nil;
//    
//    // Add this instance of TestClass as an observer of the TestNotification.
//    // We tell the notification center to inform us of "TestNotification"
//    // notifications using the receiveTestNotification: selector. By
//    // specifying object:nil, we tell the notification center that we are not
//    // interested in who posted the notification. If you provided an actual
//    // object rather than nil, the notification center will only notify you
//    // when the notification was posted by that particular object.
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveTestNotification:)
//                                                 name:@"kNetworkReachabilityChangedNotification"
//                                               object:nil];
//    
//    return self;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"History"];
    [self.tableview registerNib:[UINib nibWithNibName:@"HistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    historyContext=[UIAppDelegate managedObjectContext];
    [self loadingElementsInHistory];
    
    
    
}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInHistory];
        
        
    }
    
}
//- (void) receiveTestNotification:(NSNotification *) notification
//{
//    // [notification name] should always be @"TestNotification"
//    // unless you use this method for observation of other notifications
//    // as well.
//    
//    
//}
-(void)loadingElementsInHistory{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
    dictArray = [[NSArray alloc]init];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    NSLog(@"email id in history %@",emailIdString);
    if (emailIdString==NULL) {
        historyURL = [NSString stringWithFormat:@"getuserrecord?device_id=%@&app_name=%@&email_id_to=",[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI];
    }
    else{
        historyURL = [NSString stringWithFormat:@"getuserrecord?device_id=%@&app_name=%@&email_id_to=%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString],kAppNameAPI,emailIdString];
    }
    
        NSURL *baseURL = [NSURL URLWithString:kBaseURL];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        [manager GET:historyURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"Response :%@",responseObject);
            if ([responseObject objectForKey:@"response"]) {
                dictArray = [responseObject objectForKey:@"response"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    [self.tableview reloadData];
                });
                
                
                NSLog(@"Count in Online Service %lu",(unsigned long)dictArray.count);
                unsigned long i=[self fetchingCoreData];
                NSLog(@"Count %lu",i);
                
                if (dictArray.count<i || dictArray.count>i) {
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [self deleteAllObjects:@"HistoryInStationMaster"];
                        [dictArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            
                            [historyContext performBlockAndWait:^{
                                
                                HistoryInStationMaster * history = [NSEntityDescription insertNewObjectForEntityForName:@"HistoryInStationMaster"inManagedObjectContext:historyContext];
                                history.msgdetail = [obj objectForKey:@"msg_detail"];
                                history.locationdetail = [obj objectForKey:@"location_detail"];
                                history.createddate = [obj objectForKey:@"created_date"];
                                history.changeitid = [obj objectForKey:@"changeit_id"];
                                if ([[obj objectForKey:@"images"]isEqualToString:@""]) {
                                    //[history setValue:@"noImage" forKey:@"image"];
                                    history.image=@"noImage";
                                }
                                else{
                                    //[history setValue:[obj objectForKey:@"images"] forKey:@"image"];
                                    history.image=[obj objectForKey:@"images"];
                                }
                                
                            }];
                            
                            NSError *error;
                            
                            if (![historyContext save:&error]) {
                                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                            }
                            
                        }];
                        
                    });
                    
                }
            }
            else if (responseObject==NULL) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });

                [self deleteAllObjects:@"HistoryInStationMaster"];
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                
                [self deleteAllObjects:@"HistoryInStationMaster"];

          }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error : %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            [self deleteAllObjects:@"HistoryInStationMaster"];
        }];
    
    }
    else{
        // Test listing all Stored Messages from the DB
       
        [historyContext performBlockAndWait:^{
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryInStationMaster" inManagedObjectContext:historyContext];
            [fetchRequest setEntity:entity];
            NSError *error;
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                                initWithKey:@"createddate" ascending:NO];
            [fetchRequest setSortDescriptors:@[sortDescriptor]];
            
            dictArray = [historyContext executeFetchRequest:fetchRequest error:&error];
            NSLog(@"Count in coredata %lu",(unsigned long)dictArray.count);
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
            [self.tableview reloadData];
        });
        

}

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
        
    });
}

#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}
#pragma mark FetchingCoreData method

-(unsigned long)fetchingCoreData{
    __block NSArray * coreDataArray;
    [historyContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"HistoryInStationMaster" inManagedObjectContext:historyContext];
        [fetchRequest setEntity:entity];
        NSError *error;
        
        coreDataArray = [historyContext executeFetchRequest:fetchRequest error:&error];
        NSLog(@"Count in coredata %lu",(unsigned long)coreDataArray.count);
    }];
    
    return coreDataArray.count;
}

#pragma mark Deleting Coredata method
- (void) deleteAllObjects: (NSString *) entityDescription  {
    
    [historyContext performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:historyContext];
        [fetchRequest setEntity:entity];
        
        NSError *error;
        NSArray *items = [historyContext executeFetchRequest:fetchRequest error:&error];
        //[fetchRequest release];
        
        
        for (NSManagedObject *managedObject in items) {
            [historyContext deleteObject:managedObject];
            NSLog(@"%@ object deleted",entityDescription);
        }
        if (![historyContext save:&error]) {
            NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
        }
        
    }];
    
    
}

#pragma mark TableView Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dictArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    HistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[HistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    //HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.tag = indexPath.row;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        dictForTV = [dictArray objectAtIndex:indexPath.row];
        
        if ([[dictForTV objectForKey:@"images"] isEqualToString:@""]) {
            
            UIImage *tmpImage = [UIImage imageNamed:@"ImgNotFound.png"];
            cell.imagevw.image = tmpImage;
            
        }
        else{
            cell.imagevw.image=nil;
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator setCenter: cell.imagevw.center];
            [activityIndicator startAnimating];
            [cell.contentView addSubview:activityIndicator];
            //imageURLHist =[[NSString stringWithFormat:@"%@%@",kImageURL, [dictForTV objectForKey:@"images"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            imageURLHist= [[NSString stringWithFormat:@"%@%@",kImageURL, [dictForTV objectForKey:@"images"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageURLHist].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:imageURLHist] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }
        }
        cell.dataText.text = [dictForTV objectForKey:@"msg_detail"];
        cell.textOfLocation.text = [dictForTV objectForKey:@"location_detail"];
        cell.dateAndTime.text = [dictForTV objectForKey:@"created_date"];
        
        cell.myButton.tag= [[dictForTV objectForKey:@"changeit_id"] integerValue];
        [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendQueryButton.tag=[[dictForTV objectForKey:@"changeit_id"] integerValue];
        [cell.sendQueryButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];


    }
    else{
        
        HistoryInStationMaster *info = [dictArray objectAtIndex:indexPath.row];
        //cell.textLabel.text = obj. name;
        // Configure the cell...
        //dict = [fetchedObjects objectAtIndex:indexPath.row];
        cell.dataText.text = info.msgdetail;
        cell.textOfLocation.text = info.locationdetail;
        cell.dateAndTime.text = info.createddate;
        NSLog(@"Message %@",info.msgdetail);
        
        cell.myButton.tag= [info.changeitid integerValue];
        [cell.myButton addTarget:self action:@selector(resendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.sendQueryButton.tag=[info.changeitid integerValue];
        [cell.sendQueryButton addTarget:self action:@selector(sendQueryButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        if (![[info valueForKey:@"image"] isEqualToString:@"noImage"])
        {
            // cell.imagevw.image  = [UIImage imageWithData:info.image];
            cell.imagevw.image=nil;
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator setCenter: cell.imagevw.center];
            [activityIndicator startAnimating];
            [cell.contentView addSubview:activityIndicator];
            
            //imageURLHist =[[NSString stringWithFormat:@"%@%@",kImageURL, [info valueForKey:@"image"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            imageURLHist= [[NSString stringWithFormat:@"%@%@",kImageURL, [info valueForKey:@"image"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageURLHist].absoluteString];
            if(image == nil)
            {
                [cell.imagevw sd_setImageWithURL:[NSURL URLWithString:imageURLHist] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [cell.imagevw setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [cell.imagevw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            } else {
                [cell.imagevw setImage:image];
                [activityIndicator stopAnimating];
                //[activityIndicator removeFromSuperview];
            }
            
            
        }
        else{
            cell.imagevw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }

        
    }

    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    // hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
     //hud.dimBackground = YES;

    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            
                
                DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
                
                NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
                if ([[rowDict objectForKey:@"images"] isEqualToString:@""]) {
                    detail.imageName=@"noImage";
                }
                else{
                    detail.imageName = [rowDict objectForKey:@"images"];
                    
                }
                //NSLog(@"imagename%@Hello",imageString);
                NSLog(@"RowDict %@",rowDict);
                
                detail.date = [rowDict objectForKey:@"created_date"];
                detail.details = [rowDict objectForKey:@"msg_detail"];
                detail.locationStr=[rowDict objectForKey:@"location_detail"];
                dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:detail animated:YES];
                
                
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
            
        });

    }
    else
    {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            
                
                
                
                DetailViewController *detail = [self.storyboard instantiateViewControllerWithIdentifier:@"detail"];
                HistoryInStationMaster *info = [dictArray objectAtIndex:indexPath.row];
                //NSMutableDictionary *rowDict = [dictArray objectAtIndex:indexPath.row];
                detail.imageName = info.image;
                detail.date = info.createddate;
                detail.details = info.msgdetail;
                detail.locationStr=info.locationdetail;
                dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:detail animated:YES];

                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
            
        });

        
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

#pragma mark SendQueryMethod Button Click


-(void)sendQueryButtonClick:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    
    ChatViewController * chatController=[[ChatViewController alloc]init];
    chatController.itemID=[NSString stringWithFormat:@"%ld",(long)senderButton.tag];
    NSLog(@"sender button %@",chatController.itemID);

    dispatch_async(dispatch_get_main_queue(), ^{
    
        [self.navigationController pushViewController:chatController animated:YES];
    });
    
}

#pragma mark ResendButton Click

-(void)resendButtonClick:(id)sender{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
       
        
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Are you sure to Resend?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * resendAction=[UIAlertAction actionWithTitle:@"Resend" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
            hud.contentColor =khudColour;
            hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
            //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
            
            // Set the label text.
            hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");
            UIButton *senderButton = (UIButton *)sender;
            NSLog(@"sender button %ld",(long)senderButton.tag);
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            // getting an NSString
            NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
            
            NSString *resendString = [NSString stringWithFormat:@"resendemailbyid?changeit_id=%ld&app_name=%@&email_id_to=%@",(long)(senderButton.tag),kAppNameAPI,emailIdString];
            
            NSURL *baseURL = [NSURL URLWithString:kBaseURL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            [manager POST:resendString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"Response : %@",responseObject);
                if ([[responseObject objectForKey:@"response"]isEqualToString:@"Email sent successfully"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                    [self errorAlertWithTitle:kAppNameAlert message:@"Email Resend Successfully.If you don’t receive a confirmation email within 1 hour please contact us" actionTitle:@"Done"];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                    [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK"];
                }
                

                    
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"Error :%@",error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK"];
                
            }];


        }];
        UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil];
        
        [alert addAction:resendAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
    }
    
 

}

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

@end
