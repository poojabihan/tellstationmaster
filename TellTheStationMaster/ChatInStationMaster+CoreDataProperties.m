//
//  ChatInStationMaster+CoreDataProperties.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "ChatInStationMaster+CoreDataProperties.h"

@implementation ChatInStationMaster (CoreDataProperties)

+ (NSFetchRequest<ChatInStationMaster *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ChatInStationMaster"];
}

@dynamic changeitid;
@dynamic createddate;
@dynamic message;
@dynamic posttype;
@dynamic usertype;

@end
