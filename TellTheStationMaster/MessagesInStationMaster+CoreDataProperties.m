//
//  MessagesInStationMaster+CoreDataProperties.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "MessagesInStationMaster+CoreDataProperties.h"

@implementation MessagesInStationMaster (CoreDataProperties)

+ (NSFetchRequest<MessagesInStationMaster *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MessagesInStationMaster"];
}

@dynamic msgtype;
@dynamic msgdetail;
@dynamic lastmsg;
@dynamic createddate;
@dynamic changeitid;

@end
