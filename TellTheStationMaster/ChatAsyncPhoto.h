//
//  ChatAsyncPhoto.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import "JSQPhotoMediaItem.h"

@interface ChatAsyncPhoto : JSQPhotoMediaItem
@property (nonatomic, strong) UIImageView *asyncImageView;

- (instancetype)initWithURL:(NSURL *)URL;
@end
