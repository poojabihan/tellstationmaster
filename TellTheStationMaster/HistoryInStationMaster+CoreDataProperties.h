//
//  HistoryInStationMaster+CoreDataProperties.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "HistoryInStationMaster+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface HistoryInStationMaster (CoreDataProperties)

+ (NSFetchRequest<HistoryInStationMaster *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *msgdetail;
@property (nullable, nonatomic, copy) NSString *locationdetail;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *createddate;
@property (nullable, nonatomic, copy) NSString *changeitid;

@end

NS_ASSUME_NONNULL_END
