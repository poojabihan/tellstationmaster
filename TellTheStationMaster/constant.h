//
//  constant.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#ifndef constant_h
#define constant_h
#define UIAppDelegate \
((AppDelegate*)[[UIApplication sharedApplication]delegate])
//constants for Webservice
//demo_tellsid
//tellsid

#define kBaseURL @"http://live.thechangeconsultancy.co/tellsid/index.php/apistation/"
#define kImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/tellthestationmaster/"
#define kChatImageURL @"http://live.thechangeconsultancy.co/tellsid/assets/upload/chatimg/"

//#define kBaseURL @"http://demo.thechangeconsultancy.co/demo_tellsid/index.php/apistation/"
//#define kImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/tellthestationmaster/"
//#define kChatImageURL @"http://demo.thechangeconsultancy.co/demo_tellsid/assets/upload/chatimg/"

//#define kBaseURL @"http://tellsid.softintelligence.co.uk/index.php/apistation/"
//#define kImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/tellthestationmaster/"
//#define kChatImageURL @"http://tellsid.softintelligence.co.uk/assets/upload/chatimg/"
#define kAppNameAPI @"TellTheStationMaster"
#define kAppSecret @"stationmaster@1"


#define kAppNameAlert @"Tell TheStationMaster"
#define kInternetOff @"Internet Connection Required!"
#define khudColour [UIColor colorWithRed:48/255.0f green:135/255.0f blue:180/255.0f alpha:1]
// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#endif /* constant_h */
