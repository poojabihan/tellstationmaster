//
//  HistoryInStationMaster+CoreDataClass.h
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryInStationMaster : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "HistoryInStationMaster+CoreDataProperties.h"
